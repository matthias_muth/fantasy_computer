#!/usr/bin/env perl

use v5.26;
use feature 'signatures';
no warnings 'experimental::signatures';

use Data::Dump qw( pp );

@ARGV = "challenge.pl"
    unless @ARGV;
my $old_source = join "", <>;
my @old_code;
for ( $old_source ) {
    while( /
        ^\s* opcode \s+ => \s+ (\d+) .*?
        ^( \s* code \s* => \s* sub \s* { .*? }, ) \s* $
        /xmsg )
    {
        # my ( $opcode, $old_code ) = ( $1, $2 );
        # $old_code =~ s/code/old_code/;
        my ( $opcode, $old_code ) = ( $1, $2 );
        $old_code =~ /^(\s+)/;
        my $indent = $1;
        $old_code =~ s/^$indent//mg;

        $old_code[$opcode] = $old_code
            unless $old_code =~ /not implemented yet/i;
    }
}

# @old_code = ();         # OLD CODE IS DISABLED
pp\@old_code;

my $pc;

sub generate_ops_array {
    my %op;
    my @ops;
    my $descr;
    while ( <DATA> ) {
        chomp $_;
        # say "read ", pp $_;

        /^\s* ([A-Z]+) \s* = \s* (\d+)/x and do {
            $op{opcode} = $2;
            # say "    op: ", pp \%op;
            $ops[$op{opcode}] = { %op };
            next;
        };

        /^\s* ([A-Z]+) (?: \s+ (.*?) )? \s*$/x and do {
            @op{qw( name params descr )} =
                ( $1, [ split /[, ]+/, $2 || "" ], $descr );
            $descr = undef;
            # say "    op: ", pp \%op;
            next;
        };

        /\w/ and $descr = $_;
        # say "    descr: ", pp $descr;
    }

    my $indent = " " x 4;
    say "my \@ops = ();";
    for ( grep defined $ops[$_], 0..$#ops ) {
	all_attributes( \@ops, $_ );
	# existing_code_only( \@ops, $_ );
        say "};";
    }
}

generate_ops_array();

exit 0;

sub all_attributes( $ops_ref, $i ) {
    my ( $name, $opcode, $params, $descr ) =
	@{$ops_ref->[$i]}{qw( name opcode params descr )};
    # say "    # opcode $opcode: $name",
    #     $params ? ( " " . join ", ", @$params ) : ();
    say "\$ops[$opcode] = {";
    say "    opcode => $opcode, name => '$name',",
	$params
	? " params => [ " . join( ", ", map "'$_'", @$params ) . " ],"
	: ();
    $descr =~ s/'/\\'/g;
    say "    descr => '$descr',";
    my $op_len = 1 + ( $params ? @$params : 0 );
    my $code = "";
    my $indent = "        ";
    if ( $name =~ /^halt$/i ) {
	$code = "code => sub { return \$pc = undef },";
    }
    else {
	$code = join "\n",
	    "code => sub {",
	    $params
	    ? ( @$params == 1
		? "    my \$$params->[0] = \$mem[++\$pc];"
		: ( "    my ( " . join( ", ", map "\$$_", @$params )
		    . " ) = ( "
		    . join( ", ", map "\$mem[++\$pc]", @$params )
		    . " );" ) )
	    : (),
	    $old_code[$_]
	    ? "    # USE EXISTING IMPLEMENTATION!"
	    : ( "    # NOT IMPLEMENTED YET.\n"
		. "    say STDERR \"ERROR: $name (opcode $opcode) not implemented yet.\";\n"
		. "    return undef;" ),
	    "    return ++\$pc;",
	    "},";
    }

    $code =~ s/^/    /mg;
    say $code;

    my $old_code = $old_code[$i];
    if ( my $old_code = $old_code[$i] ) {
	$old_code =~ s/^/    /mg;
	if ( $old_code ne $code ) {
	    $old_code =~ s/code/old_code/;
	    say $old_code;
	}
    }
}

sub existing_code_only( $ops_ref, $i ) {
    my ( $name, $opcode, $params, $descr ) =
	@{$ops_ref->[$i]}{qw( name opcode params descr )};
    # say "    # opcode $opcode: $name",
    #     $params ? ( " " . join ", ", @$params ) : ();

    my $code = $old_code[$i];
    $code =~ s/code => //;
    $code =~ s/^/    /mg;
    say "    # opcode $opcode: $name", $params ? " @{$params}" : "";
    say $code;
}

1;

__DATA__

== opcode listing ==
Loads the value from regsrc into regdst. E.g. regdst = regsrc

MOVR reg_dst, reg_src
MOVR = 10

Loads the numeric value into register regdst. E.g. regdst = value

MOVV reg_dst, value
MOVV = 11

Adds the value from regsrc to the value of regdst and store the result in reg_dst

ADD reg_dst, reg_src
ADD = 20

Subtracts the value of regsrc from the value of regdst and store the result in reg_dst

SUB reg_dst, reg_src
SUB = 21

Pushes the value of reg_src on the stack

PUSH reg_src
PUSH = 30

Pops the last value from stack and loads it into register reg_dst

POP reg_dst
POP = 31

Jumps the execution to address addr. Similar to a GOTO!

JP addr
JP = 40

Jump to the address addr only if the value from reg1 < reg2 (IF reg1 < reg2 THEN JP addr)

JL reg_1, reg_2, addr
JL = 41

Pushes onto the stack the address of instruction that follows CALL and then jumps to address addr

CALL addr
CALL = 42

Pops from the stack the last number, assumes is an address and jump to that address

RET
RET = 50

Print on the screen the value contained in the register reg

PRINT reg
PRINT = 60

Stops our VM. The virtual CPU doesn't execute instructions once HALT is encountered.

HALT
HALT = 255
