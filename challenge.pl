#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

my @mem = ();
my @reg = ();
my @stack;
my $pc = 0;

my @ops = ();
$ops[10] = {
    opcode => 10, name => 'MOVR', params => [ 'reg_dst', 'reg_src' ],
    descr => 'Loads the value from regsrc into regdst. E.g. regdst = regsrc',
    code => sub {
        my ( $reg_dst, $reg_src ) = ( $mem[++$pc], $mem[++$pc] );
        $reg[$reg_dst] = $reg[$reg_src];
        return ++$pc;
    },
};
$ops[11] = {
    opcode => 11, name => 'MOVV', params => [ 'reg_dst', 'value' ],
    descr => 'Loads the numeric value into register regdst. E.g. regdst = value',
    code => sub {
        my ( $reg_dst, $value ) = ( $mem[++$pc], $mem[++$pc] );
        $reg[$reg_dst] = $value;
        return ++$pc;
    },
};
$ops[20] = {
    opcode => 20, name => 'ADD', params => [ 'reg_dst', 'reg_src' ],
    descr => 'Adds the value from regsrc to the value of regdst and store the result in reg_dst',
    code => sub {
        my ( $reg_dst, $reg_src ) = ( $mem[++$pc], $mem[++$pc] );
        $reg[$reg_dst] += $reg[$reg_src];
        return ++$pc;
    },
};
$ops[21] = {
    opcode => 21, name => 'SUB', params => [ 'reg_dst', 'reg_src' ],
    descr => 'Subtracts the value of regsrc from the value of regdst and store the result in reg_dst',
    code => sub {
        my ( $reg_dst, $reg_src ) = ( $mem[++$pc], $mem[++$pc] );
        $reg[$reg_dst] -= $reg[$reg_src];
        return ++$pc;
    },
};
$ops[30] = {
    opcode => 30, name => 'PUSH', params => [ 'reg_src' ],
    descr => 'Pushes the value of reg_src on the stack',
    code => sub {
        my $reg_src = $mem[++$pc];
        push @stack, $reg[$reg_src];
        return ++$pc;
    },
};
$ops[31] = {
    opcode => 31, name => 'POP', params => [ 'reg_dst' ],
    descr => 'Pops the last value from stack and loads it into register reg_dst',
    code => sub {
        my $reg_dst = $mem[++$pc];
        $reg[$reg_dst] = pop @stack;
        return ++$pc;
    },
};
$ops[40] = {
    opcode => 40, name => 'JP', params => [ 'addr' ],
    descr => 'Jumps the execution to address addr. Similar to a GOTO!',
    code => sub {
        my $addr = $mem[++$pc];
        return $pc = $addr;
    },
};
$ops[41] = {
    opcode => 41, name => 'JL', params => [ 'reg_1', 'reg_2', 'addr' ],
    descr => 'Jump to the address addr only if the value from reg1 < reg2 (IF reg1 < reg2 THEN JP addr)',
    code => sub {
        my ( $reg_1, $reg_2, $addr ) =
            ( $mem[++$pc], $mem[++$pc], $mem[++$pc] );
        return $reg[$reg_1] < $reg[$reg_2] ? ( $pc = $addr ) : ++$pc;
    },
};
$ops[42] = {
    opcode => 42, name => 'CALL', params => [ 'addr' ],
    descr => 'Pushes onto the stack the address of instruction that follows CALL and then jumps to address addr',
    code => sub {
        my $addr = $mem[++$pc];
        push @stack, ++$pc;
        return $pc = $addr;
    },
};
$ops[50] = {
    opcode => 50, name => 'RET', params => [  ],
    descr => 'Pops from the stack the last number, assumes is an address and jump to that address',
    code => sub {
        return $pc = pop @stack;
    },
};
$ops[60] = {
    opcode => 60, name => 'PRINT', params => [ 'reg' ],
    descr => 'Print on the screen the value contained in the register reg',
    code => sub {
        my $reg = $mem[++$pc];
        say $reg[$reg];
        return ++$pc;
    },
};
$ops[255] = {
    opcode => 255, name => 'HALT', params => [  ],
    descr => 'Stops our VM. The virtual CPU doesn\'t execute instructions once HALT is encountered.',
    code => sub { return $pc = undef },
};

# Build the memory from __DATA__.
@mem = split /[, ]+/, join "", map { chomp $_; $_ } <DATA>;

while ( 1 ) {
    my $opcode = $mem[$pc];
    &{$ops[$opcode]{code}}
        or last;
}

1;

__DATA__
11,0,10,42,6,255,30,0,11,0,0,11,1,1,11,3,1,60,1,10,2,0,20, 2,1,60,2,10,0,1,10,1,2,11,2,1,20,3,2,31,2,30,2,41,3,2,19,31,0,50
