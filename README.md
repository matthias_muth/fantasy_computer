# Fantasy Computer
This implements the little 'Fantasy Computer' challenge as announced on [Reddit](https://www.reddit.com/r/adventofcode/comments/128t3c6/puzzle_implement_a_fantasy_computer_to_find_out/).

The implementation is in Perl, done by Matthias Muth.
